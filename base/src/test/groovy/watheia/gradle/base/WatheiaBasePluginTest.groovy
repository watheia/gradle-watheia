/**
 * Copyright (C) 2021 Watheia Labs, LLC
 * All righs reserved.
 */
package watheia.gradle.base

import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import spock.lang.Specification

/**
 *
 */
class WatheiaBasePluginTest extends Specification {

    def plugin() {
        given:
        def project = ProjectBuilder.builder().build()

        when:
        project.plugins.apply('org.watheia.base')

        then:
        project.tasks.findByName('stage') != null
    }

}
