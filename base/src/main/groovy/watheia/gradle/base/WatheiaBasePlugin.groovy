/**
 * Copyright (C) 2021 Watheia Labs, LLC
 * All righs reserved.
 */
package watheia.gradle.base

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task

/**
 * .
 */
class WatheiaBasePlugin implements Plugin<Project> {

	protected Task assemble = null
	protected Task build = null
	protected Task clean = null
	protected Task check = null
	protected Task projectReport = null

	void apply(Project project) {
		project.pluginManager.apply('base')
		project.pluginManager.apply('project-report')

		// Grab references to existing tasks
		assemble = project.tasks.getByName('assemble')
		check = project.tasks.getByName('check')
		clean = project.tasks.getByName('clean')
		build = project.tasks.getByName('build')
		projectReport = project.tasks.getByName('projectReport')

		assemble.dependsOn(projectReport)

		//Ensure parallele builds play nice
		[
			assemble,
			build,
			check,
			projectReport
		].each { Task task ->
			task.mustRunAfter(clean)
		}

		registerTasks(project)
	}

	protected void registerTasks(Project project) {
		// Create stage task
		project.tasks.register('stage') {
			doLast {
				println("Staging Complete.")
			}
			dependsOn clean, assemble, check, projectReport
		}
	}

}
