/**
 * Copyright (C) 2021 Watheia Labs, LLC
 * All righs reserved.
 */
package watheia.gradle.base

import org.gradle.testkit.runner.GradleRunner

import spock.lang.Specification

// @CompileStatic
/**
 * Base plugin for managing build life-cycle
 */
class WatheiaBasePluginFunctionalTest extends Specification {

	void helloBasePlugin() {
		given:
		def projectDir = new File('build/functionalTest')
		projectDir.mkdirs()
		new File(projectDir, 'settings.gradle').text = ''
		new File(projectDir, 'build.gradle').text = """
            plugins {
                id('org.watheia.base')
            }
        """

		when:
		GradleRunner runner = GradleRunner.create()
		runner.forwardOutput()
		runner.withPluginClasspath()
		runner.withArguments('stage')
		runner.withProjectDir(projectDir)
		def result = runner.build()

		then:
		result.output.contains("Staging Complete.")
	}

}
