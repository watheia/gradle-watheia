ARG run_image="node:12"
ARG build_image="gradle:6.7-jdk11"
ARG build_flags="--no-build-cache --no-daemon"

FROM ${build_image} as stage

# provide a binding for the workspace dir
VOLUME /wa
WORKDIR /wa

# Prepare for staging
ADD . .
RUN ./gradlew stage  ${build_flags}

FROM ${run_image}

COPY --from=stage "/wa/base/build/reports/" /stage/reports/base/
COPY --from=stage "/wa/screenplay/build/reports/" /stage/reports/screenplay/

# Set port for Auto-DevOps
ENV PORT 5000
EXPOSE 5000

CMD [ "npx", "serve", "-S", "/stage"


